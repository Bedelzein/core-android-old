package kz.bedelzein.core.android

import android.content.Intent

object Requirements {

    object NonActivityIntent {
        /**
         * Calling startActivity() from outside of an Activity context
         * requires the [Intent.FLAG_ACTIVITY_NEW_TASK]
         * */
        const val ACTIVITY_FLAG_FOR_NAVIGATION = Intent.FLAG_ACTIVITY_NEW_TASK
    }
}
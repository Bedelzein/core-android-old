package kz.bedelzein.core.android

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.annotation.MainThread

const val EMPTY_INTENT_FLAGS = 0

@MainThread
fun Context.toast(message: String, isLong: Boolean = false) = Toast.makeText(
    this,
    message,
    if (isLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
).show()

@MainThread
inline fun <reified T : Activity> Context.navigateTo(vararg flags: Int) {
    val intent = Intent(this, T::class.java)
    val optionalFlags = flags
        .takeIf { it.isNotEmpty() }
        ?.reduce { accumulator, flag -> accumulator or flag }
        ?: EMPTY_INTENT_FLAGS
    val defaultFlag = EMPTY_INTENT_FLAGS.takeIf { this is Activity }
        ?: Requirements.NonActivityIntent.ACTIVITY_FLAG_FOR_NAVIGATION
    intent.flags = defaultFlag or optionalFlags
    startActivity(intent)
}
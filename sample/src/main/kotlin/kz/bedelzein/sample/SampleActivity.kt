package kz.bedelzein.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.bedelzein.core.android.toast

class SampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toast("Hello, World!", isLong = true)
    }
}